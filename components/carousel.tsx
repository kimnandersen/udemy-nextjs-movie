
export type CarouselImages = {
  title: string,
  imageUrl: string,
}

export interface CarouselParams {
  children?: Element, 
  name: string,
  images: Array<CarouselImages>,
}

export default function Carousel(params: CarouselParams) {
  const carouselName: string = "carousel-" + params.name;

  return (
  <>
  <div id={carouselName} className="carousel slide my-4" data-bs-ride="carousel">
    <ol className="carousel-indicators">
      {params.images.map((image: CarouselImages, index: number) => 
        <li data-bs-target={"#" + carouselName} data-bs-slide-to={index} className={index == 0 ? "active" : ""} key={carouselName + index}></li>
      )} 
  </ol>
    <div className="carousel-inner" role="listbox">
      {params.images.map((image: CarouselImages, index: number) => 
        <div className={index == 0 ? 'carousel-item active' : 'carousel-item'} key={carouselName + index + 100}>
          <img className="d-block img-fluid" src={image.imageUrl} alt={image.title} />
        </div>
          )}
    </div>
  <a className="carousel-control-prev" href={"#" + carouselName} role="button" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </a>
  <a className="carousel-control-next" href={"#" + carouselName} role="button" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </a>
  </div>
  </>
  );
};