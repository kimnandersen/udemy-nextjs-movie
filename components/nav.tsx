
import Link from 'next/link'

export type NavbarLinks = {
    title: string,
    href: string,
    key: string,
};

export interface NavbarParams {
    children?: Element,
    links: Array<NavbarLinks>,
};

export default function Navbar(params: NavbarParams) {
    if (!params.links || params.links.length < 1) {
        return (
            <>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                <a className="navbar-brand" href="#">Start Bootstrap</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                            <Link href="#home">
                                <a className="nav-item">Home</a>
                            </Link>
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
            </>
        );
    } else {
        return (
            <>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                <div className="container">
                <a className="navbar-brand" href="#">Start Bootstrap</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                    {
                        params.links.map((link: NavbarLinks,index: number) =>
                            <li className={index == 0 ? "nav-item active" : "nav-item"} key={link.key}>
                                <Link href={link.href}>
                                    <a className="nav-link">{link.title}</a>
                                </Link>
                            </li>
                        )
                    }
                    </ul>
                </div>
                </div>
            </nav>
            </>
        );
    }
}